close all; clear all; clc;
avgPicoScopeDataVoltage = [];
startingIdx = 3;
endIndex = 15;
inputPower = linspace(0,6,13);
for i=startingIdx:endIndex
    filename = sprintf('test%i.mat', i)
    load(filename);
    %figure;
    %plot(A);
    
    windowWidth = 15000;
    kernel = ones(windowWidth,1) / windowWidth;
    filtered_A = filter(kernel, 1, A);
    
    %figure;
    %plot(filtered_A);
    
    [pks, lcs] = findpeaks(filtered_A, 'MinPeakDistance',0.2e+5); 
    
    figure;
    plot(filtered_A); 
    hold on
    plot(lcs,pks,'o');
    hold off;
    
    avgPicoScopeDataVoltage(i-startingIdx+1) = sum(pks)/length(pks);
end

%%
refPicoscopeVoltage = [95,260,408,564,690,847,990,1150,1260,1400];
refVoltage = [1.62,1.65,1.67,1.69,1.7,1.71,1.72,1.74,1.74,1.75];
refCurrent = [0.96, 1.98,3.02,4.04,4.96,5.98,7.02,8.05,8.96,9.95];

linregVoltage = polyfit(refPicoscopeVoltage,refVoltage,1);
linregCurrent = polyfit(refPicoscopeVoltage,refCurrent,1);

figure;
plot(refPicoscopeVoltage, refVoltage,'o');
hold on
xLimits = get(gca,'XLim');
plot(xLimits, linregVoltage(1)*xLimits+linregVoltage(2),'--');
hold off;
title('refPicoscope Voltage vs refVoltage')

figure;
plot(refPicoscopeVoltage, refCurrent,'o');
hold on
xLimits = get(gca,'XLim');
plot(xLimits, linregCurrent(1)*xLimits+linregCurrent(2),'--');
hold off;
title('refPicoscope Voltage vs refCurrent')


%% map the averageVoltage to the picoscope voltage/current

mappedVoltage = avgPicoScopeDataVoltage * linregVoltage(1) + linregVoltage(2);
mappedCurrent = avgPicoScopeDataVoltage * linregCurrent(1) + linregCurrent(2);

%power 
power = (mappedVoltage.* mappedCurrent)
figure;
plot(inputPower,power,'x')

