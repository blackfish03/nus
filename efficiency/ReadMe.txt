
test 1: 1.6-1.7GHz; 6 dBm
test 2: 3 dBm
test 3 lighton: 0
--- light off ---
test3: 0;  -10.782
test4: 0.5; -11.896
test5: 1; -13.228
test6: 1.5; -15.072
test7: 2; -17.473
test8: 2.5; -20.556
test9: 3; -25.252
test10: 3.5, -35.337
test11: 4, -25.258
test12: 4.5; s11 = -25.31dB
test13: 5; s11 = -20.776
test14: 5.5; s11 = -17.957
test15: 6 dBm; s11: -15.881 dB

test17: -0.5
test18: -1
