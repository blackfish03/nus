clear all; close all; clc;
%{
load('power_21_same_height_lpf_50.mat');
%}
load('same_height_pw25_2.mat');
endIndex = length(A)-20000; 
startIndex = 10000;
len = endIndex-startIndex+1;
time = linspace(0,len,len);
intensity = A(startIndex:endIndex);

%{
filtered_intensity = LowPass_A_50_(startIndex:endIndex);


figure;
plot(time*Tinterval, intensity);
hold on;
plot(time*Tinterval, filtered_intensity);
hold off;
title('LPF @ 50Hz');
%}

windowWidth = 15000;
kernel = ones(windowWidth,1) / windowWidth;
filtered_A = filter(kernel, 1, intensity);

figure;
startIndex = 40000;
plot((time(startIndex:len)-startIndex)*Tinterval, filtered_A(startIndex:len));
title('Filtered');
xlabel('Time (s)');
ylabel('Voltage (V)');
ylim([0.5 6.5])
xlim([0 19])