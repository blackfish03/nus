close all; clear all; clc
M = readtable('WaveguideFreqSweep.csv');
figure;
plot(M.Freq,M.Current);
xlim([1.9, 2.3])
xlabel('Freq (MHz)')
ylabel('Current (mA)')

figure;
plot(M.Freq,M.Current_1);
xlim([1.9, 2.3])
xlabel('Freq (MHz)')
ylabel('Current (mA)')