
clear all; close all; clc;
load('pm-13-3.mat');
% A is the pulse data

endIndexA = length(A)-10000; % discarding the drop off in the end
startIdx = 858000; % discarding the irregular pulses upfront
time = linspace(0,endIndexA,endIndexA);
time = (time(startIdx:endIndexA) - time(startIdx))*Tinterval;

A = A(startIdx:endIndexA);
B = B(startIdx:endIndexA);

windowWidth = 15000; %15000 almost filters out the blip on the rising edge
windowWidth_B = 9000; %15000 almost filters out the blip on the rising edge
kernel = ones(windowWidth,1) / windowWidth;
kernel_B = ones(windowWidth_B,1) / windowWidth_B;
filtered_A = filter(kernel, 1, A);
filtered_B = filter(kernel_B, 1, B);
%% display filtered data

figure;
plot(time,A);
hold on
plot(time,filtered_A);
hold off;
legend('Raw Pulse Data', 'Filtered Data');
title('Filtering Pulse Data');
xlabel('Time (s)');
ylabel('Voltage (V)');
%%
figure;
plot(time,B);
hold on
plot(time,filtered_B);
hold off;
legend('Raw ECG Data', 'Filtered Data');
xlabel('Time (s)');
ylabel('Voltage (V)');
dataLength = length(time);
figure;
title('Filtered Pulse and ECG Data');
start=10000;
subaxis(2, 1, 1, 'sh', 0.03, 'sv', 0.01, 'padding', 0, 'margin', 0);
plot(time(start:dataLength),filtered_A(start:dataLength),'DisplayName','Pulse Data');
subaxis(2, 1, 2, 'sh', 0.03, 'sv', 0.01, 'padding', 0, 'margin', 0);
plot(time(start:dataLength),filtered_B(start:dataLength), 'DisplayName','ECG Data');
xlabel('Time (s)');
ylabel('Voltage (V)');

figure;
title('Filtered Pulse and ECG Data');
start=10000;
subplot(2,1,1);
plot(time(start:dataLength),filtered_A(start:dataLength),'DisplayName','Pulse Data');
xlabel('Time (s)');
ylabel('Voltage (V)');
subplot(2, 1, 2);
plot(time(start:dataLength),filtered_B(start:dataLength), 'DisplayName','ECG Data');
xlabel('Time (s)');
ylabel('Voltage (V)');

figure;
title('Pulse and ECG Data');
start=10000;
plot(time(start:dataLength),filtered_A(start:dataLength),'DisplayName','Pulse Data');
hold on
moved_B = filtered_B(start:dataLength) - 0.03;
plot(time(start:dataLength),moved_B, 'DisplayName','ECG Data');
xlabel('Time (s)');
set(gca,'YTickLabel',[]);
ylabel('Amplitude (a.u.)');
hold off;
%% find peaks for Pulse
[pks, lcs] = findpeaks(filtered_A, 'MinPeakDistance',140000); 
% each pulse is about 163000 points long

figure;
plot(time,filtered_A,lcs*Tinterval,pks,'o');
hold on
% extracting data from each peak
overlayStartingIndices = lcs - 85000;
overlayEndingIndices = lcs + 46000;

lengthA = length(A);
if overlayEndingIndices(end) >= lengthA
    overlayEndingIndices(end) = lengthA;
end

plot(overlayStartingIndices*Tinterval,filtered_A(overlayStartingIndices),'x');
plot(overlayEndingIndices*Tinterval,filtered_A(overlayEndingIndices),'+');
hold off;

%% find peaks for ECG
[pks_ecg, lcs_ecg] = findpeaks(filtered_B, 'MinPeakDistance',140000); 
% each pulse is about 163000 points long

figure;
plot(time,filtered_B,lcs_ecg*Tinterval,pks_ecg,'o');
hold on
% extracting data from each peak
overlayStartingIndices_ecg = lcs_ecg - 85000;
overlayEndingIndices_ecg = lcs_ecg + 46000;

lengthB = length(B);
if overlayEndingIndices_ecg(end) >= lengthB
    overlayEndingIndices_ecg(end) = lengthB;
end

plot(overlayStartingIndices_ecg*Tinterval,filtered_B(overlayStartingIndices_ecg),'x');
plot(overlayEndingIndices_ecg*Tinterval,filtered_B(overlayEndingIndices_ecg),'+');
hold off;

%% overlaying everything
figure;
for i=1:length(lcs)
    modifiedTime = time(overlayStartingIndices(i):overlayEndingIndices(i))-time(overlayStartingIndices(i));
    plot(modifiedTime, filtered_A(overlayStartingIndices(i):overlayEndingIndices(i)), 'DisplayName',sprintf('Pulse %f', num2str(int8(i))));
    i=i+1;
    hold on
end
hold off;
title('Pulses');
xlabel('Time (s)');
ylabel('Voltage (V)');

figure;
for i=1:length(lcs_ecg)
    modifiedTime = time(overlayStartingIndices_ecg(i):overlayEndingIndices_ecg(i))-time(overlayStartingIndices_ecg(i));
    plot(modifiedTime, filtered_B(overlayStartingIndices_ecg(i):overlayEndingIndices_ecg(i)), 'DisplayName',sprintf('Pulse %f', num2str(int8(i))));
    i=i+1;
    hold on
end
hold off;
title('ECG');
xlabel('Time (s)');
ylabel('Voltage (V)');