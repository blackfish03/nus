import numpy as np
import math
import matplotlib.pyplot as plt
import json
import pdb

""" this script is for the preliminary data analysis for the time vs packet index """
""" The graph shows the packet index and the total lapse time between with/without waveguide and their variance"""

def calcStuff(WOWG, numExp):
	WOWG_std = []
	i=0
	trialCount = len(WOWG[0])
	exp = np.zeros((trialCount,numExp), dtype='int')
	lapseTime = np.zeros(trialCount, dtype = 'int')
	avg = np.zeros(trialCount, dtype='int')
	totalTime = 0
	while i < trialCount: # loops through the number of trials (along the exp)
		for x in expIdx: # loops through the number of experiments 
			x = x - 1
			exp[i][x]= WOWG[x][i]

		WOWG_std.append(np.std(exp[i]))
		avg[i] = np.average(exp[i])
		totalTime += avg[i]
		lapseTime[i] = totalTime
		
		i+=1

	
	print (WOWG_std)
	return lapseTime, WOWG_std, avg


filename = 'TimeVSPackCount.json'
numExp = 3
expIdx = np.linspace(1, numExp, numExp, dtype='int')
my_json = json.load(open(filename))

WOWG = []
for x in expIdx:
	text = "exp" + str(x)
	WOWG.append(my_json["WOWG"][text])

WWG = []
for x in expIdx:
	text = "exp" + str(x)
	WWG.append(my_json["WWG"][text])

packetCount = len(WOWG[0])
packetIdx = np.linspace(0,packetCount-1,packetCount, dtype='int')

lapseTime_WO, WOWG_std, WOWG_avg = calcStuff(WOWG, numExp)
lapseTime_W, WWG_std, WWG_avg = calcStuff (WWG, numExp)


plt.fill_between(packetIdx, lapseTime_WO - WOWG_std, lapseTime_WO + WOWG_std,color='xkcd:wheat',label='WOWG STD', alpha=0.3)
plt.plot(packetIdx, lapseTime_WO, marker='o', color='xkcd:red',label='WOWG Data')

plt.fill_between(packetIdx, lapseTime_W - WWG_std, lapseTime_W + WWG_std,color='xkcd:azure',label='WWG STD', alpha=0.5)
plt.plot(packetIdx, lapseTime_W, marker='o', color='xkcd:blue', label='WWG Data')

plt.legend()
plt.xlabel('PacketIdx (#)')
plt.ylabel('Time (ms)')
plt.title('PacketIdx vs Lapse Time')
#plt.step(packetIdx, lapseTime, where='post')
#plt.errorbar(packetIdx, lapseTime, yerr=WOWG_std, color='g', ls='None')
plt.show()