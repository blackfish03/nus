import os
import pdb
import numpy as np
from matplotlib import pyplot as plt
import csv

""" this script plots the power level vs latency and the variance"""

Px_wwg = np.array([-55,-30,-20,-16,-12,-8,-4,0,4])
Px_wowg = np.array([-20,-16,-12,-8,-4,0,4])

numData = 7
index = np.linspace(1,numData,num=numData,dtype=int)
avg = []
wowg_std = []
for i in index:
	filename = ("WOWG-%s" %(str(i)))
	lines = open(filename+".txt").read().splitlines()
	times = []
	csv = open(filename+".csv", "w")
	header = "Px,%s\n" %(Px_wowg[(i-1)])
	csv.write(header)
	legend = "Index, Latency (ms)\n"
	csv.write(legend)
	packetCounter = 0
	for s in lines:
		if s.find("(") != -1 and s.find("KB") == -1:
			times.append(int(s[s.find("(")+2:s.find(")")-2]))
			lineToWrite = "%s, %s\n" %(packetCounter, times[-1])
			packetCounter+=1
			csv.write(lineToWrite)

	avg.append(round(np.average(times),2))
	wowg_std.append(np.std(times))
#print("WOWG")	
#print(avg)

numData = 9
index = np.linspace(1,numData,num=numData,dtype=int)
avg_w = []
wwg_std = []
for i in index:
	filename = ("WWG-%s" %(str(i)))
	lines = open(filename+".txt").read().splitlines()
	times = []
	csv = open(filename+".csv", "w")
	header = "Px,%s\n" %(Px_wwg[(i-1)])
	csv.write(header)
	legend = "Index, Latency (ms)\n"
	csv.write(legend)
	packetCounter = 0
	for s in lines:
		if s.find("(") != -1 and s.find("KB") == -1:
			times.append(int(s[s.find("(")+2:s.find(")")-2]))
			lineToWrite = "%s, %s\n" %(packetCounter, times[-1])
			packetCounter+=1
			csv.write(lineToWrite)

	avg_w.append(round(np.average(times),2))
	wwg_std.append(np.std(times))
#print("WWG")	
#print(avg_w)

plt.plot(Px_wowg, avg, marker='o', label='WOWG')
plt.errorbar(Px_wowg, avg, yerr=wowg_std, color='xkcd:azure', ls='None', label='WOWG STD')
#plt.fill_between(Px_wowg, np.array(avg)-np.array(wowg_std), np.array(avg)+np.array(wowg_std),color='xkcd:azure',label='WOWG STD', alpha=0.3)
plt.plot(Px_wwg, avg_w, marker='x', label='WWG')
plt.errorbar(Px_wwg, avg_w, yerr=wwg_std, color='xkcd:wheat', ls='None', label='WWG STD')
#plt.fill_between(Px_wwg, np.array(avg_w)-np.array(wwg_std), np.array(avg_w)+np.array(wwg_std),color='xkcd:wheat',label='WOWG STD', alpha=0.3)
plt.xlabel('Px')
plt.ylabel('Averaged Latency (ms)')
plt.legend()
plt.title('Transmit Power vs Latency')
plt.show()