import pdb
from imutils import contours
from skimage import measure
from scipy import stats
import numpy as np
import argparse
import imutils
import cv2
import math
import datetime
from matplotlib import pyplot as plt

def pre_proc(image):
	#converts to grayscale img
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	#apply gaussian blur to reduce noise
	gray = cv2.GaussianBlur(gray, (blurr_radius, blurr_radius), 0)
	return gray

def processBWImage(thresh):
	""" takes in an image (perferably subtracted with baseline) and erode/dilate it"""
	# low light condition might need to adjust this threshold for picking up the max value
	bound = int(round(thresh.reshape((thresh.shape[0]*thresh.shape[1])).max()*thresholdLim))
	thresh = cv2.threshold(thresh, bound, 255, cv2.THRESH_BINARY)[1]
	thresh = cv2.erode(thresh, None, iterations=2)
	thresh = cv2.dilate(thresh, None, iterations=4)
	return thresh

def findMask(thresh):
	# perform a connected component analysis on the thresholded
	# image, then initialize a mask to store only the  largest component
	labels = measure.label(thresh, neighbors=8, background=0)
	mask = np.zeros(thresh.shape, dtype="uint8")

	prevNumPixels = 0
	prevMask = np.zeros(thresh.shape, dtype="uint8")
	wasWritten = False
	# loop over the unique components
	for label in np.unique(labels):
		# if this is the background label, ignore it
		if label == 0:
			continue

		# otherwise, construct the label mask and count the
		# number of pixels 
		labelMask = np.zeros(thresh.shape, dtype="uint8")
		labelMask[labels == label] = 255
		numPixels = cv2.countNonZero(labelMask)

		# if the number of pixels in the component is sufficiently
		# large, then add it to our mask of "large blobs"
		if inCalibration:
			if numPixels > 500 and numPixels > prevNumPixels:
				wasWritten = True
				mask = cv2.subtract(mask, prevMask)
				mask = cv2.add(mask, labelMask)

			if wasWritten:
				prevNumPixels = numPixels
				prevMask = labelMask
				wasWritten = False
		else:
			if numPixels > 500:
				mask = cv2.add(mask, labelMask)
	return mask

def findCircle(image,mask):
	# find the contours in the mask
	cX = 0
	cY = 0
	radius = 0

	cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	#cnts = contours.sort_contours(cnts)[0]
	area = 0
	# loop over the contours
	for (i, cnt) in enumerate(cnts):
		# draw the bright spot on the image
		(x, y, w, h) = cv2.boundingRect(cnt)
		((cX, cY), radius) = cv2.minEnclosingCircle(cnt)
		cv2.circle(image, (int(cX), int(cY)), int(radius),
			(0, 0, 255), 3)
		cv2.putText(image, "#{}".format(i + 1), (x, y - 15),
			cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
		area = round(area + math.pow(radius,2) * math.pi,2)
		cv2.putText(image, str(area), (x - 30, y + 15),
			cv2.FONT_HERSHEY_SIMPLEX, 6, (255, 0, 0), 5)
	time = datetime.datetime.now().time()
	cv2.namedWindow("Image %s" %(time), cv2.WINDOW_NORMAL)
	cv2.imshow("Image %s" %(time), image)
	#cv2.imwrite(filename + 'processed.png',image)
	return cX, cY, radius

""" Parameters to tune """
#number of calibration images 
numCalibImgs = 4 
#can play around with this parameter if the circles didn't find the light properly
thresholdLim = 0.4 

#measured intensity with the optical power meter
measuredIntensity = [0.96, 5.11, 15.03, 24.56] 
#measured current
current = [0.04, 0.23, 0.67, 1.14] 

image_of_interest_filename = "5.jpg" #the image we want to know the intensity of

""" Section 1 Calibration """
blurr_radius = 29
filename = "0.jpg"
base = cv2.imread(filename, 1)
base_orig = base.copy()
base_gray = pre_proc(base)
inCalibration = True


i = 1
pixelIntensity = []
while i <= numCalibImgs:
	filename = ("%s.jpg" %i)
	print(filename)
	first = cv2.imread(filename, 1)
	first_copy = first.copy()
	first_gray = pre_proc(first)

	subtract = cv2.subtract(first_gray,base_gray)
	gray = processBWImage(subtract)
	mask = findMask(gray)
	cX, cY, radius = findCircle(first_copy, mask)
	boolMask = [mask==255]
	pixelIntensity.append(np.sum(first_gray[boolMask]))
	i+=1
print("Pixel Intensity: %s" %(pixelIntensity))



# histogram plotting
"""
hist_nomask = cv2.calcHist([first_gray.astype('float32')], [0], None, [256],[0,256])
hist = cv2.calcHist([first_gray.astype('float32')], [0], mask.astype('uint8'),[256],[0,256])
plt.plot(hist, label='Masked region')
plt.plot(hist_nomask, label='No mask')
plt.xlim([0,256])
plt.legend()
plt.title('Histogram')
#plt.savefig(filename+"-hist.png")
"""

""" Section 2 mapping power to measured results"""

inCalibration = False


img = cv2.imread(image_of_interest_filename, 1)
img_org = img.copy()
img_gray = pre_proc(img)

thresh = processBWImage(img_gray)
mask = findMask(thresh)
cX, cY, radius = findCircle(img_org, mask)

boolMask = [mask==255]
#pixel to power
intensity = np.sum(img_gray[boolMask])
slope_pixel, intercept_pixel, r_value_pixel, p_value_pixel, std_err_pixel = stats.linregress(pixelIntensity, measuredIntensity)
#pixel to current
slope, intercept, r_value, p_value, std_err = stats.linregress(current, measuredIntensity)

print("Intensity: %s" %(intensity))

mappedPower = float(intensity * slope_pixel + intercept_pixel)
print("Mapped Power is (uW): %s" %(mappedPower))

fig = plt.figure
ax1 = plt.subplot(2,1,1)
plt.scatter(pixelIntensity,measuredIntensity, c='r', label='calibration data')
plt.ylabel('Intensity (μW)')
plt.xlabel('Pixel Intensity')
fit = np.polyfit(pixelIntensity,measuredIntensity,1)
plt.plot(intensity, mappedPower,'.',color='xkcd:wheat',markersize=12,label='picture to compare')
axes = plt.gca()
x_vals = np.array(axes.get_xlim())
y_vals = intercept_pixel + slope_pixel * x_vals
plt.plot(x_vals, y_vals, '--', label="Regression line for calibration data")
plt.legend()
plt.title('Pixel Intensity, Optical Power Meter Measurement, Current')

plt.subplot(2,1,2)
plt.scatter(current, measuredIntensity, c='r', label='calibration data')
plt.xlabel('Current (mA)')
plt.ylabel('Intensity (μW)')
guessed_current = (mappedPower - intercept)/slope
plt.plot(guessed_current, mappedPower,'.',color='xkcd:wheat',markersize=12, label='picture to compare')
axes = plt.gca()
x_vals = np.array(axes.get_xlim())
y_vals = intercept + slope * x_vals
plt.plot(x_vals, y_vals, '--', label="Regression line for calibration data")
plt.legend()

plt.show()
#cv2.waitKey(0)
